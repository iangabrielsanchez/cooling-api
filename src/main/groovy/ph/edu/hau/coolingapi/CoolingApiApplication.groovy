package ph.edu.hau.coolingapi

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class CoolingApiApplication {

	static void main(String[] args) {
		SpringApplication.run CoolingApiApplication, args
	}
}
