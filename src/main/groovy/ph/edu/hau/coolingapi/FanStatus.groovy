package ph.edu.hau.coolingapi

class FanStatus {
    String name
    boolean isRunning
    boolean isToggledManually

    FanStatus(String name, boolean running, boolean toggled){
        this.name = name
        isRunning = running
        isToggledManually = toggled
    }

}
