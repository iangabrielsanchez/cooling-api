package ph.edu.hau.coolingapi.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ph.edu.hau.coolingapi.repository.TemperatureEntry
import ph.edu.hau.coolingapi.repository.TemperatureRepository

import java.lang.reflect.Field

@Service
class TemperatureServiceImpl implements TemperatureService {
    @Autowired
    private TemperatureRepository repository

    @Override
    TemperatureEntry saveTemperatureEntry(TemperatureEntry temperatureEntry) {
        return repository.save(temperatureEntry)
    }

    @Override
    Iterable<TemperatureEntry> getTemperatureEntries() {
        return repository.findAll()
    }

    @Override
    Optional<TemperatureEntry> getTemperatureEntry(long id) {
        return repository.findById(id)
    }

    @Override
    TemperatureEntry updateTemperatureEntry(long id, TemperatureEntry temperatureEntry) {
        updateUserInfo(repository.findById(id).get(), temperatureEntry);
        return repository.save(temperatureEntry);
    }

    @Override
    boolean deleteTemperatureEntry(long id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
            return true;
        }
        return false;
    }

    private TemperatureEntry updateUserInfo(TemperatureEntry oldEntry, TemperatureEntry newEntry) {

        for (Field field : newEntry.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                Object fieldValue = field.get(newEntry);
                if (fieldValue == null) {
                    Object oldValue = field.get(oldEntry);
                    field.set(newEntry, oldValue);
                }
            } catch (IllegalAccessException ex) {
                //logger.fatal( "Failed to update user info.", ex );
            }
        }
        return newEntry;
    }
}
