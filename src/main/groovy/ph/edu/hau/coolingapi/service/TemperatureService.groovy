package ph.edu.hau.coolingapi.service

import ph.edu.hau.coolingapi.repository.TemperatureEntry
import ph.edu.hau.coolingapi.repository.TemperatureRepository

interface TemperatureService {

    public TemperatureEntry saveTemperatureEntry(TemperatureEntry temperatureEntry );

    public Iterable<TemperatureEntry> getTemperatureEntries();

    public Optional<TemperatureEntry> getTemperatureEntry( long id );

    public TemperatureEntry updateTemperatureEntry( long id, TemperatureEntry temperatureEntry );

    public boolean deleteTemperatureEntry( long id );

}
