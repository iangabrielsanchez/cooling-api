package ph.edu.hau.coolingapi

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import ph.edu.hau.coolingapi.repository.TemperatureEntry
import ph.edu.hau.coolingapi.service.TemperatureService

@Controller
class TemperatureController {
    @Autowired
    private TemperatureService service

    @PostMapping("/temp/save")
    ResponseEntity<TemperatureEntry> saveTemp(@RequestBody TemperatureEntry temperatureEntry) {
        return handle(service.saveTemperatureEntry(temperatureEntry), HttpStatus.ACCEPTED)
    }

    @GetMapping("/temp")
    ResponseEntity<Iterable<TemperatureEntry>> getAllTemp(){
        return handle(service.getTemperatureEntries(), HttpStatus.OK)
    }


    private ResponseEntity<?> handle(Object entry, HttpStatus status){
        return new ResponseEntity(entry, status);
    }
}
