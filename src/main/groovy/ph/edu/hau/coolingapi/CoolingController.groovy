package ph.edu.hau.coolingapi

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import ph.edu.hau.coolingapi.repository.TemperatureEntry
import ph.edu.hau.coolingapi.service.TemperatureService

@Controller
class CoolingController {
    private static CarStatus carStatus
    private static Thread timerThread
    @Autowired
    private TemperatureService service

    static {
        carStatus = new CarStatus(false, 0, 35)
    }

    private static void startThread(int seconds) {
        timerThread = new Thread({
            Thread.sleep(seconds * 1000L)
            carStatus.isFanOn = false
        })
        timerThread.start()
    }

    private static void stopThread() {
        timerThread.interrupt()
    }

    @PostMapping("/temperature")
    ResponseEntity<CarStatus> setStatus(Float temperature) {
        carStatus.carTemperature = temperature
        TemperatureEntry entry = new TemperatureEntry()
        entry.setTemperature(temperature)
        entry.setThreshold(carStatus.targetTemp)
        entry.setFanStatus(carStatus.isFanOn)
        service.saveTemperatureEntry(entry)
        return getStatus()
    }

    @PostMapping("/fan/toggle")
    ResponseEntity<CarStatus> toggleFans() {
        carStatus.isFanOn = !carStatus.isFanOn
        if (carStatus.isFanOn) {
            startThread(600)
        }
        else{
            stopThread()
        }
        return getStatus()
    }

    @PostMapping("/temperature/target")
    ResponseEntity<CarStatus> setTargetTemp(Float targetTemp) {
        carStatus.targetTemp = targetTemp
        return getStatus()
    }

    @GetMapping("/status")
    ResponseEntity<CarStatus> getStatus() {
        return new ResponseEntity<CarStatus>(carStatus, HttpStatus.OK)
    }

}
