package ph.edu.hau.coolingapi

class CarStatus {

    boolean isFanOn
    float carTemperature
    float targetTemp

    CarStatus(boolean isFanOn, float carTemperature, float targetTemp) {
        this.isFanOn = isFanOn
        this.carTemperature = carTemperature
        this.targetTemp = targetTemp
    }


}
