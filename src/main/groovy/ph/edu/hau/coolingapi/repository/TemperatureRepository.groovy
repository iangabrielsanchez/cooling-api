package ph.edu.hau.coolingapi.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface TemperatureRepository extends CrudRepository<TemperatureEntry, Long> {
}
