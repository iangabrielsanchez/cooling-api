package ph.edu.hau.coolingapi.repository

import org.springframework.data.annotation.CreatedDate

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.PrePersist
import java.time.LocalDateTime

@Entity
class TemperatureEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id
    private Float temperature
    @CreatedDate
    private LocalDateTime timestamp
    private Float threshold
    private Boolean fanStatus

    Long getId() {
        return id
    }

    void setId(Long id) {
        this.id = id
    }

    Float getTemperature() {
        return temperature
    }

    void setTemperature(Float temperature) {
        this.temperature = temperature
    }

    LocalDateTime getTimestamp() {
        return timestamp
    }

    @PrePersist
    void setTimestamp() {
        this.timestamp = LocalDateTime.now()
    }

    Float getThreshold() {
        return threshold
    }

    void setThreshold(Float threshold) {
        this.threshold = threshold
    }

    Boolean getFanStatus() {
        return fanStatus
    }

    void setFanStatus(Boolean fanStatus) {
        this.fanStatus = fanStatus
    }
}
